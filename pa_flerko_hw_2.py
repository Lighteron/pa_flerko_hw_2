# 1. Установить git. Результат: git --version
# 2. Зарегистрироваться на github.com или другом аналогичном сервисе. Результат: ссылка
# 3. Создать view-функцию, которая возвращает содержимое файла с установленными пакетами в текущем проекте
# (Pipfile.lock)
#  get_pipfile() -> 127.0.0.1:5000/pipfile
# 4. Создать view-функцию, которая возвращает список случайных студентов. Использовать библиотеку faker.
# get_random_students() -> 127.0.0.1:5000/random_students
# 5. [необязательно] Создать view-функцию, которая будет возвращать средний рост и средний вес
# (в см и кг соответственно) для студентов из файла hw.csv
#  get_avr_data() -> 127.0.0.1:5000/avr_data

from faker import Faker
from flask import Flask
import csv

app = Flask(__name__)


@app.route("/pipfile")
def get_pipfile():
    with open("Pipfile.lock", "r") as file:
        return file.read()


@app.route("/random_students")
def get_random_students():
    name_lst = []
    fake = Faker("UK")
    for i in range(10):
        name = fake.first_name()
        name_lst.append(name)
    return "<br>".join(name_lst)


@app.route("/avr_data")
def get_avr_data():
    INCH_TO_CM = 2.54
    POUNDS_TO_KG = 0.453592

    with open("hw.csv") as file:
        reader = list(csv.DictReader(file))
        total_height = 0
        total_weight = 0

        for row in reader[1:-1]:
            total_height += float(row[' "Height(Inches)"'])
            total_weight += float(row[' "Weight(Pounds)"'])
        avg_height = round(total_height / int(row["Index"]) * INCH_TO_CM)
        avg_weight = round(total_weight / int(row["Index"]) * POUNDS_TO_KG)

        return f'Average height: {avg_height} cm, Average weight: {avg_weight} kg'


app.run(debug=True)
